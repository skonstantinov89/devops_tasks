import sys
from django.conf import settings
# SETTINGS PART
settings.configure(
		DEBUG = True,
		SECRET_KEY = 'this_is_the_secret_key',
		ROOT_URLCONF = __name__,
		MIDDLEWARE_CLASSES = (  'django.middleware.common.CommonMiddleware',
								# 'django.middleware.csrf.CsrfViewMiddleware',
								'django.middleware.clickjacking.XFrameOptionsMiddleware',
								),
	)
# END SETTINGS

# MODEL PART
import sqlite3
DB_NAME = 'test.db'
# Create table
# conn = sqlite3.connect(DB_NAME)
# c = conn.cursor()
# try:
# 	c.execute('''CREATE TABLE main
#              (key text, space text, ip text)''')
# except sqlite3.OperationalError:
# 	print ('Base already created')
# conn.commit()
# conn.close()
#END MODEL

# URL/VIEW MANAGMENT PART
from django.conf.urls import url
from django.http import HttpResponse, Http404, HttpResponseNotAllowed, HttpResponseBadRequest
import hashlib
import hmac
from ast import literal_eval

import json
def checker(request):
	key = 'this_is_the_secret_key'

	if request.method == 'POST':
		jsonData_unicode = request.body.decode('utf-8')
		jsonData = json.loads(jsonData_unicode)

		try:
			hmacKey = request.META['HTTP_AUTHORIZATION']
		except:
			return HttpResponseBadRequest('No HMAC key!', status = 401)
		ipAddr = request.META['REMOTE_ADDR']
		hmacKeyToVerify = hmac.new(key, jsonData, hashlib.sha256).hexdigest()
		if hmacKey == hmacKeyToVerify:
			jsonData = literal_eval(jsonData)
			jsonData['key'] = jsonData['key'].strip('\n')
			jsonData['df_free'] = jsonData['df_free'].strip('\n')
			jsonData['hmac'] = jsonData['hmac'].strip('\n')
			
			conn = sqlite3.connect(DB_NAME)
			c = conn.cursor()
			c.execute('''INSERT INTO main(key, space, ip)
                   VALUES(?,?,?)''', (jsonData['key'], jsonData['df_free'], ipAddr))
			conn.commit()
			conn.close()
			return HttpResponse('', status = 201)
		else:
			return HttpResponseBadRequest('Incorrect HMAC Keys', status = 401)

	# return HttpResponseBadRequest('Not supported request type', status = 503)
	return HttpResponse('Not supported request type')

urlpatterns = (
	url(r'^$', checker),
	)

# END URL/VIEW

#STARTUP CONFIGS
from django.core.wsgi import get_wsgi_application
application=get_wsgi_application()

if __name__ == "__main__":
	from django.core.management import execute_from_command_line
	execute_from_command_line(sys.argv)



