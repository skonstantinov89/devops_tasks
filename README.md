##Requirements
> Python 2.7+, not Python3 cuz of dependency issues

> Tested on XUbuntu 14.04

##Installation
> 1. Install python-pip (debs - sudo apt-get install python-pip)
> 2. Install Django webFramework - sudo pip install django
## Configure cmdline script
> 1. Change the DEBUG value to True
> 2. Change the URL value to the current location where the web API script is running

##Start Web server (default run)
> 1. python devops_api_lightweight.py runserver 0.0.0.0:8000

##Start web server(alternative preferable run):
> 1. Install Gunicorn (sudo pip install gunicorn)
> 2. gunicorn devops_api_lightweight --log-file=- --daemon

##My NGINX instance (sites-enabled/default)
```
 upstream backend  {
  server localhost:8000;
}

server {
        listen 80;
        listen [::]:80;
        listen   443 default ssl;

        server_name sconsttech.eu;

	#Self-signed certificate
       ssl_certificate /etc/nginx/ssl/nginx.crt;
        ssl_certificate_key /etc/nginx/ssl/nginx.key;

       location / {
               proxy_pass http://backend;
                # try_files $uri $uri/ =404;
        }
	#force redirect HTTP to HTTPS
    if ($ssl_protocol = "") {
       rewrite ^   https://sconsttech.eu$request_uri? permanent;
    }

}
```