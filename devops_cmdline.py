from os import listdir, system
import subprocess
import sys
import hashlib
import hmac
import os
import json
import requests
DEBUG = True
key = 'this_is_the_secret_key'
URL = 'https://sconsttech.eu/'

def findFiles():
    try:
        if DEBUG:
            files = listdir('/home/jovial/.ssh')
        else:
            files = listdir('/root/.ssh')
    except OSError, error:
        if 'Errno 13' in str(error):
            print ('Please run the command as root!')
            return None
        if 'Errno 2' in str(error):
            print ('No such location and/or files!')
            return None
    return files
def findPub(files):
    for eachFile in files:
        if eachFile.lower().endswith('.pub'):
            if DEBUG:
                fileOpened = open('/home/jovial/.ssh/'+str(eachFile), 'r')
            else:
                fileOpened = open('/root/.ssh/'+str(eachFile), 'r')
            fileRead = fileOpened.read()
            return str(fileRead)
    print('No .pub file found!')
    return None
def findFree():
    batcmd="df -m / | awk -F ' ' '{print $4}'| sed -n 2p"
    result = subprocess.check_output(batcmd, shell=True)
    return str(result)

def sendRequest(url, jsonData,verify, hmacKey=None):
    headers = {'Authorization': hmacKey}
    r = requests.post(url, json=jsonData, headers=headers,verify=verify)

if __name__ == "__main__":
    try:
        files = findFiles()
        pubKey = findPub(files)
    except:
        print ('No .pub files or no .pub content')
        sys.exit()
    jsonData = {}
    if files == None and pubKey == None:
        print('Correct the problems and try again!')
        sys.exit()
    else:
        df_free = findFree()
        jsonData['key'] = pubKey
        jsonData['df_free'] = df_free
        jsonData['hmac'] = str(pubKey) + str(df_free)
        jsonData = json.dumps(jsonData)
        hmacKey = hmac.new(key, jsonData, hashlib.sha256).hexdigest()
        
        # mytest.com is mapped on my /etc/hosts
        # sendRequest(url='http://mytest.com/', jsonData=jsonData, hmacKey=hmacKey)

        #verify=False, an ugly hack to bypass the Self-sign SSL security warning/error
        sendRequest(url=URL, jsonData=jsonData, hmacKey=hmacKey, verify=False)
